<?php
include 'db_connect.php'; 
include 'history.php';// Include your database connection file

// Check if the form is submitted
if ($_SERVER["REQUEST_METHOD"] == "POST") {
    // Retrieve form data
    $Pencode = $_POST['Pencode'];
    $Description = $_POST['Description'];
    $Lec = $_POST['Lec'];
    $Lab = $_POST['Lab'];
    $Prerequisite = $_POST['Prerequisite'];

    // Prepare insert statement
    $stmt = $conn->prepare("INSERT INTO subjects (Pencode, `Description`, Lec, Lab, Prerequisite) 
                            VALUES (?, ?, ?, ?, ?)");

    // Bind parameters
    $stmt->bind_param("sssss", $Pencode, $Description, $Lec, $Lab, $Prerequisite);
    
    // Execute statement
    if ($stmt->execute()) {
        // Query executed successfully
     
        echo 1; // Return 1 for success
    } else {
        // Error in query execution
        echo 0; // Return 0 for failure
    }

    // Close prepared statement
    $stmt->close();

    // Close database connection
    $conn->close();
}
?>
