
<?php
// Include your database connection
include 'db_connect.php';

function set_grade($grade, $status, $pencodes, $conn){
    // Initialize the query string
    
    $qry = "UPDATE studentcurriculumsubject SET"; 

    // Add grade and status to the query if they are provided
    if($grade !== null){
        $qry .= " Grade = ?,";
    }
    if($status !== null){
        $qry .= " Status = ?,";
    } 

    // Remove the trailing comma if any
    $qry = rtrim($qry, ",");

    // Add condition for Pencodes
    $qry .= " WHERE ";
$conditions = [];
    foreach($pencodes as $pencode){
        $conditions[] = " SubjectID IN (SELECT SubjectID FROM subjects WHERE Pencode = ?)";
     }
    $qry .= implode(" OR ", $conditions);

    // Prepare the SQL statement
    $stmt = $conn->prepare($qry);

    // Bind parameters
    $bindTypes = "";
    $bindParams = [];
    if($grade !== null){
        $bindTypes .= "d";
        $bindParams[] = &$grade;
    }
    if($status !== null){
        $bindTypes .= "i";
        $bindParams[] = &$status;
    }
    foreach($pencodes as $pencode){
        $bindTypes .= "s";
        $bindParams[] = &$pencode;
    }
    if(!empty($bindParams)){
        // Bind parameters dynamically
        $bindParams = array_merge([$bindTypes], $bindParams);
        call_user_func_array([$stmt, "bind_param"], $bindParams);
    }
    
    // Return the prepared statement object
    return $stmt;
} 



?>