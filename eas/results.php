<?php
// Connect to the database
$conn = mysqli_connect("localhost", "root", "", "eas_db");

// Check the connection
if (!$conn) {
    die("Connection failed: " . mysqli_connect_error());
}

// Retrieve student information
$result = mysqli_query($conn, "SELECT s.id AS id, s.student_code, s.firstname, s.middlename, s.lastname, s.year, sp.track
                               FROM students s
                               LEFT JOIN specialization sp ON s.track_id = sp.id");

$studentsData = [];

if ($result && mysqli_num_rows($result) > 0) {
    while ($row = mysqli_fetch_assoc($result)) {
        $studentInfo = [
            'id' => $row['id'], // Include the id value from students table
            'student_code' => $row['student_code'],
            'firstname' => $row['firstname'],
            'middlename' => $row['middlename'],
            'lastname' => $row['lastname'],
            'year' => $row['year'],
            'track' => $row['track'] // Adding track information
        ];

        $studentsData[] = [
            'studentInfo' => $studentInfo,
            'tableData' => [
                'id' => $row['id']
            ]
        ];
    }
} else {
    $studentsData[] = ["No student data found."];
}

// Close the connection
mysqli_close($conn);
?>

<div class="col-lg-12">
    <div class="card card-outline card-secondary">
        <div class="card-header">
            
        </div>
        <div class="card-body">
            <table class="table table-hover table-bordered" id="list">
                <colgroup>
                    <col width="5%">
                    <col width="15%">
                    <col width="25%">
                    <col width="25%">
                    <col width="15%">
                </colgroup>
                <thead>
                    <tr>
                        <th class="text-center">#</th>
                        <th>Student Code</th>
                        <th>Name</th>
                        <th>Curriculum Year</th>
                        <th>Track</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                <?php 
                $i = 1; // Initialize $i variable
                foreach ($studentsData as $data):
                    if (!empty($data['studentInfo'])): 
                ?>
                    <tr>
                        <td class="text-center"><?php echo $i++; ?></td>
                        <td><b><?php echo "{$data['studentInfo']['firstname']} {$data['studentInfo']['middlename']} {$data['studentInfo']['lastname']}"; ?></b></td>
                        <td><b><?php echo $data['studentInfo']['student_code']; ?></b></td>
                        <td><b><?php echo $data['studentInfo']['year']; ?></b></td>
                        <td><b><?php echo $data['studentInfo']['track']; ?></b></td>
                        <td class="text-center">
                            <div class="content-container">
                                <div class="actions">
                                <?php
                                    // Calculate the years dynamically based on the student's year information
                                    $currentYear = (int)date('Y');
                                    $studentYear = (int)$data['studentInfo']['year'];
                                    $nextYear = $currentYear + 1;

                                    // Determine the correct year to display based on the student's year
                                    if ($studentYear >= 2021 && $studentYear < 2022) {
                                        echo "<a href='./index.php?page=advise&id=".$data['studentInfo']['id']."' class='btn btn-primary btn-sm' title='Advise'><i class='fas fa-pen'></i></a>";
                                    }
                                ?>
                                </div>
                                <hr>
                            </div>
                        </td>
                    </tr>
                <?php endif; ?>
                <?php endforeach; ?>
                </tbody>
            </table>
        </div>
    </div>
</div>
<style>
    table td{
        vertical-align: middle !important;
    }
    </style>
<script>
    $(document).ready(function(){
        $('#list').dataTable()
        $('.view_student').click(function(){
            uni_modal("Student's Details","view_student.php?id="+$(this).attr('data-id'),"large")
        })
        $('.delete_student').click(function(){
            _conf("Are you sure to delete this Student?","delete_student",[$(this).attr('data-id')])
        })
    })
   
</script>
