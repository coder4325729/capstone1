<?php
include 'db_connect.php'; // Include your database connection file
include 'history.php';
include 'query.php';
// Check if the form is submitted
if ($_SERVER["REQUEST_METHOD"] == "POST") {
    // Retrieve form data
    $SubjectID = $_POST['SubjectID'];
    $Pencode = $_POST['Pencode'];
    $Description = $_POST['Description'];
    $Lec = $_POST['Lec'];
    $Lab = $_POST['Lab'];
    $Prerequisite = $_POST['Prerequisite'];

    // Prepare update statement
    $query = "SELECT * FROM subjects WHERE SubjectID = $SubjectID";
    $result = mysqli_query($conn, $query);
    $row = mysqli_fetch_array($result);
    $_subjectid = $row['SubjectID'];
    $_pencode = $row['Pencode'];
    $_description = $row['Description'];
    $_lec = $row['Lec'];
    $_lab = $row['Lab'];
    $_prerequisite = $row['Prerequisite'];

    $stmt = $conn->prepare("UPDATE subjects SET 
                            Pencode = ?, 
                            `Description` = ?, 
                            Lec = ?, 
                            Lab = ?, 
                            Prerequisite = ? 
                            WHERE SubjectID = ?");

    // Bind parameters
    $stmt->bind_param("sssssi", $Pencode, $Description, $Lec, $Lab, $Prerequisite, $SubjectID);

    // Execute statement
    if ($stmt->execute()) {
        // Query executed successfully
       
        echo 1; // Return 1 for success
    } else {
        // Error in query execution
        echo 0; // Return 0 for failure
    }

    // Close prepared statement
    $stmt->close();

    // Close database connection
    $conn->close();
}
?>
