<?php
include 'db_connect.php';
if(isset($_GET['SubjectID'])){
	$qry = $conn->query("SELECT * FROM subjects where SubjectID ={$_GET['SubjectID']}")->fetch_array();
	foreach($qry as $k => $v){
		$$k = $v;
	}
}
?>
<div class="container-fluid">
	<form action="" id="manage-subject">
		<input type="hidden" name="SubjectID" value="<?php echo isset($SubjectID) ? $SubjectID : '' ?>">
		<div id="msg" class="form-group"></div>
		
		<div class="form-group">
			<label for="subject" class="control-label">Pen Code</label>
			<input type="text" class="form-control form-control-sm" name="Pencode" id="Pencode" value="<?php echo isset($Pencode) ? $Pencode : '' ?>">
		</div>
		<div class="form-group">
			<label for="description" class="control-label">Description</label>
			<textarea name="Description" id="Description" cols="30" rows="4" class="form-control"><?php echo isset($Description) ? $Description : '' ?></textarea>
		</div>
		
		<div class="form-group">
			<label for="Lec" class="control-label">Lec</label>
			<input type="int" class="form-control form-control-sm" name="Lec" id="Lec" value="<?php echo isset($Lec) ? $Lec : '' ?>">
		</div>
		<div class="form-group">
			<label for="Lab" class="control-label">Lab</label>
			<input type="int" class="form-control form-control-sm" name="Lab" id="Lab" value="<?php echo isset($Lab) ? $Lab : '' ?>">
		</div>
		<div class="form-group">
			<label for="Prerequisite" class="control-label">Pre-requisite</label>
			<input type="text" class="form-control form-control-sm" name="Prerequisite" id="Prerequisite" value="<?php echo isset($Prerequisite) ? $Prerequisite : '' ?>">
		</div>
	</form>
</div>
<div class="modal fade" id="successModal" tabindex="-1" aria-labelledby="successModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="successModalLabel">Success</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                Data successfully saved.
            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function () {
        $('#manage-subject').submit(function (e) {
            e.preventDefault();
            start_load();
            $.ajax({
                url: 'process6.php',
                method: 'POST',
                data: $(this).serialize(),
                success: function (resp) {
					console.log(resp);
                    if (resp == 1) {
                        // Data successfully updated
                        $('#successModal').modal('show');
                        setTimeout(function () {
                            $('#successModal').modal('hide');
                            location.reload(); // Reload the page after 2 seconds
                        }, 1000);
                    } else {
                        // Error in updating data
                        alert('Error updating record. Please try again.');
                    }
                    end_load();
                }
            });
        });
    });
</script>