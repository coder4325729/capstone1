<!DOCTYPE html>
<html lang="en">
<?php 
session_start();
include('./db_connect.php');
  ob_start();
  if(!isset($_SESSION['system'])){

    $system = $conn->query("SELECT * FROM system_settings")->fetch_array();
    foreach($system as $k => $v){
      $_SESSION['system'][$k] = $v;
    }
   }
  ob_end_flush();
?>
<head>
  <meta charset="utf-8">
  <meta content="width=device-width, initial-scale=1.0" name="viewport">

  <title>Login | <?php echo $_SESSION['system']['name'] ?></title>
    

  <style>

        body {
           background-color: #2F2E2E !important;  Dark background color */
            color: #fff; /* Light text color */
            margin: 0;
            padding: 0;
            display: flex;
            justify-content: center;
            align-items: center; /* Center items vertically */
            min-height: 100vh; /* Ensure the body covers the viewport height */
            background: url("Log_in.png") top center no-repeat; /* Place the background image at the top center */
            background-size: 225px;
            padding-top: 185px; /* Add padding-top to create space below the image */
        } 

        main#main { 
            color: #eeefefa4;
            width: 100%;
            max-width: 600px; /* Set a maximum width for the content */
            padding: 60px; /* Add padding to center the content */
    
        }
        .title{
           text-align: center;
        }

        .form-group label {
            color: #737373; /* Change label text color to black */
        }

     
    .btn-primary {
             /* Match the button color of the landing page */
            border: 2px;
            border-color: white;
            border-radius: 5px;
            transition: all 0.3s ease-in-out;
        }

        .btn-primary:hover {
        background-color: darkgray;
        transform: scale(1.15); /* Increase size on hover */
    }

    </style>


 
  <?php include('./header.php'); ?>
  <?php 
  if(isset($_SESSION['login_id']))
  header("location:index.php?page=home");

  ?>

</head>

<body>
    <main id="main">
      <div class="title">
        <h4 style="color: white;"><?php echo $_SESSION['system']['name'] ?> - Admin</h4>
        </div>
        <div id="login-center">
            <div class="card" style=" border-radius: 20px;">
                <div class="card-body">
                    <form id="login-form">
                        <div class="form-group">
                            <label for="username">Username</label>
                            <input type="text" id="username" name="username" class="form-control form-control-sm">
                        </div>
                        <div class="form-group">
                            <label for="password">Password</label>
                            <input type="password" id="password" name="password" class="form-control form-control-sm">
                        </div>
                        <div class="w-100 d-flex justify-content-center align-items-center">
                            <button class="btn-primary" style="padding: 8px 24px 8px 24px; border-radius: 15px; background-color: black; border: none;">Login</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </main>

    <a href="#" class="back-to-top"><i class="icofont-simple-up"></i></a>
</body>
<?php include 'footer.php' ?>
<script>
  $('#view_result').click(function(){
    $('#view_student_results').modal('show')
  })
    $('#login-form').submit(function(e){
    e.preventDefault()
    $('#login-form button[type="button"]').attr('disabled',true).html('Logging in...');
    if($(this).find('.alert-danger').length > 0 )
      $(this).find('.alert-danger').remove();
    $.ajax({
      url:'ajax.php?action=login',
      method:'POST',
      data:$(this).serialize(),
      error:err=>{
        console.log(err)
    $('#login-form button[type="button"]').removeAttr('disabled').html('Login');

      },
      success:function(resp){
        console.log(resp)
        if(resp == 1){
          location.href ='index.php?page=home';
        }else{
          $('#login-form').prepend('<div class="alert alert-danger">Username or password is incorrect.</div>')
          $('#login-form button[type="button"]').removeAttr('disabled').html('Login');
        }
      }
    })
  })
  $('#vsr-frm').submit(function(e){
    e.preventDefault()
   start_load()
    if($(this).find('.alert-danger').length > 0 )
      $(this).find('.alert-danger').remove();
    $.ajax({
      url:'ajax.php?action=login2',
      method:'POST',
      data:$(this).serialize(),
      error:err=>{
        console.log(err)
        end_load()
      },
      success:function(resp){
        if(resp == 1){
          location.href ='student_results.php';
        }else{
          $('#login-form').prepend('<div class="alert alert-danger">Student ID # is incorrect.</div>')
           end_load()
        }
      }
    })
  })
    $('.number').on('input keyup keypress',function(){
        var val = $(this).val()
        val = val.replace(/[^0-9 \,]/, '');
        val = val.toLocaleString('en-US')
        $(this).val(val)
    })
</script>  
</html>