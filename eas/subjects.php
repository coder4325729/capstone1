<?php include 'db_connect.php' ?>
<div class="col-lg-12">
	<div class="card card-outline card-secondary">
		<div class="card-header">
			<div class="card-tools">
				<a class="btn btn-block btn-sm btn-default btn-flat border-secondary new_subject" href="javascript:void(0)"><i class="fa fa-plus"></i> Add New</a>
			</div>
		</div>
		<div class="card-body">
			<div class="table-responsive"> <!-- Wrap the table with this div -->
				<table class="table tabe-hover table-bordered" id="list">
					<colgroup>
						<col width="10%">
						<col width="15%">
						<col width="30%">
						<col width="10%">
						<col width="10%">
						<col width="15%">
						<col width="10%">
					</colgroup>
					<thead>
						<tr>
							<th class="text-center">#</th>
							
							<th>Subjects</th>
							<th>Subject Title</th>
							<th>Lec</th>
							<th>Lab</th>
							<th>Pre-requisite</th>
							<th>Action</th>
						</tr>
					</thead>
					<tbody>
						<?php
						$i = 1;
						$qry = $conn->query("SELECT * FROM subjects order by unix_timestamp(date_created) desc ");
						while($row= $qry->fetch_assoc()):
						?>
						<tr>
							<th class="text-center"><?php echo $i++ ?></th>
							<td><b><?php echo ucwords($row['Pencode']) ?></b></td>
							<td><b><?php echo ucwords($row['Description']) ?></b></td>
							<td><b><?php echo ucwords($row['Lec']) ?></b></td>
							<td><b><?php echo ucwords($row['Lab']) ?></b></td>
							<td><b><?php echo ucwords($row['Prerequisite']) ?></b></td>
							<td class="text-center">
			                    <div class="btn-group">
			                        <a href="javascript:void(0)" data-id='<?php echo $row['SubjectID'] ?>' class="btn btn-primary btn-flat manage_subject">
			                          <i class="fas fa-edit"></i>
			                        </a>
			                        <button type="button" class="btn btn-danger btn-flat delete_subject" data-id="<?php echo $row['SubjectID'] ?>">
			                          <i class="fas fa-trash"></i>
			                        </button>
			                      </div>
							</td>
						</tr>	
					<?php endwhile; ?>
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>
<script>
    $(document).ready(function () {
        $('#list').dataTable();
        
        // Click event for new subject button
        $(document).on('click', '.new_subject', function () {
            uni_modal("New Subject", "new_subject.php");
        });

        // Click event for managing subject
		$(document).on('click', '.manage_subject', function () {
            var subjectID = $(this).attr('data-id');
            uni_modal("Manage Subject", "manage_subject.php?SubjectID=" + subjectID);
        });

        // Click event for deleting subject
        $(document).on('click', '.delete_subject', function () {
            var subjectID = $(this).attr('data-id');
            _conf("Are you sure to delete this Subject?", "delete_subject", [subjectID]);
        });
    });

    // Function to handle subject deletion
    function delete_subject(subjectID) {
        start_load();
        $.ajax({
            url: 'ajax.php?action=delete_subject',
            method: 'POST',
            data: { SubjectID: subjectID },
            success: function (resp) {
                if (resp == 1) {
                    alert_toast("Data successfully deleted", 'success');
                    setTimeout(function () {
                        location.reload();
                    }, 1500);
                }
            }
        });
    }
</script>