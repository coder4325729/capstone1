<?php
// Include your database connection file
include 'db_connect.php';

// Check if the form is submitted
if ($_SERVER["REQUEST_METHOD"] == "POST") {
    // Retrieve form data
    $syear = $_POST['syear'];
    $descriptive = $_POST['descriptive'];
    // Since the semester value will be determined dynamically based on the database, we don't need to retrieve it here
    
    // Perform SQL query to check if semester 9 exists for the given year
    $query_check_semester = "SELECT COUNT(*) AS semester_count FROM curriculum WHERE syear = $syear AND semester = 9";
    $result_check_semester = $conn->query($query_check_semester);

    // Check if the query was successful
    if ($result_check_semester) {
        $row_check_semester = $result_check_semester->fetch_assoc();
        $semester_count = $row_check_semester['semester_count'];
        
        // Determine the next semester value based on whether semester 9 exists
        if ($semester_count > 0) {
            // Semester 9 exists, so find the maximum semester value for the given year
            $query_max_semester = "SELECT MAX(semester) AS max_semester FROM curriculum WHERE syear = $syear";
            $result_max_semester = $conn->query($query_max_semester);

            if ($result_max_semester) {
                $row_max_semester = $result_max_semester->fetch_assoc();
                $next_semester = $row_max_semester['max_semester'] + 1; // Increment the maximum semester value by 1
            } else {
                echo "Error retrieving maximum semester: " . $conn->error;
                exit(); // Exit script if error occurs
            }
        } else {
            // Semester 9 does not exist, so set the next semester value as 9
            $next_semester = 9;
        }
        
        // Perform SQL query to insert data into the curriculum table
        $query = "INSERT INTO curriculum (syear, descriptive, semester) VALUES ('$syear', '$descriptive', '$next_semester')";
        $result = $conn->query($query);

        // Check if the query was successful
        if ($result) {
            echo "Data successfully saved.";
        } else {
            echo "Error inserting into curriculum: " . $conn->error;
        }
    } else {
        echo "Error checking semester: " . $conn->error;
    }

    // Close database connection
    $conn->close();
}
?>
