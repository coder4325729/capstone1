<div class="col-lg-12">
    <div class="card card-outline card-secondary">
        <div class="card-body">
            <form id="curriculum-form" method="post">
                <input type="hidden" name="CurriculumID">
                <div class="row">
                    <div class="col-md-6">
                        <div id="msg" class=""></div>
                        <div class="form-group text-dark">
                            <div class="form-group">
                                <label for="" class="control-label">Curriculum Year #</label>
                                <input type="number" value="2024" class="form-control form-control-sm" name="syear">
                            </div>
                        </div>
                        <div class="form-group text-dark">
                            <div class="form-group">
                                <label for="" class="control-label">Track Name</label>
                                <input type="text" class="form-control form-control-sm" name="descriptive" placeholder="Example: System Development">
                            </div>
                        </div>
                        <div class="form-group text-dark">
                            <!-- Remove the select input for semester here -->
                        </div>
                    </div>
                </div>
            </form>
        </div>

        <!-- Success Modal -->
        <div class="modal fade" id="successModal" tabindex="-1" aria-labelledby="successModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="successModalLabel">Success</h5>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body">
                        Data successfully saved.
                    </div>
                </div>
            </div>
        </div>

        <script>
            $(document).ready(function(){
                $('#curriculum-form').submit(function(e){
                    e.preventDefault();
                    start_load();
                    var formData = $('#curriculum-form').serialize();
                    console.log(formData);
                    $.ajax({
                        url: 'process5.php',
                        method: 'POST',
                        data: formData,
                        success: function(response){
                            $('#successModal').modal('show');
                            setTimeout(function(){
                                location.reload(); // Reload the page after 2 seconds
                            }, 1000);
                            end_load();
                        }
                    });
                });
            });
        </script>
