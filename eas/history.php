<?php
include 'db_connect.php';

function get_history($conn, $user_id){
    $qry = "Select * FROM audit WHERE ModifiedBy = $user_id";
    return $qry;
}


function log_history($conn, $table_id, $content_id, $action, $old_data, $new_data, $user_id, $modified_date) {
    // Check if a record with the same ContentID already exists
    $check_query = "SELECT * FROM audit WHERE ContentID = '$content_id'";
    $check_result = $conn->query($check_query);

    if ($check_result->num_rows > 0) {
        // If a record exists, perform an update instead of an insert
        $update_query = "UPDATE audit SET Action = '$action', OldData = '$old_data', NewData = '$new_data', ModifiedBy = '$user_id', ModifiedDate = '$modified_date' WHERE ContentID = '$content_id'";
        $update_result = $conn->query($update_query);

        if (!$update_result) {
            echo "Error updating record: " . $conn->error;
            exit();
        }
    } else {
        // If no record exists, perform an insert
        $insert_query = "INSERT INTO audit (TableID, ContentID, Action, OldData, NewData, ModifiedBy, ModifiedDate) VALUES ('$table_id', '$content_id', '$action', '$old_data', '$new_data', '$user_id', '$modified_date')";
        $insert_result = $conn->query($insert_query);

        if (!$insert_result) {
            echo "Error inserting record: " . $conn->error;
            exit();
        }
    }
}

// function get_user_activity(){
//     $qry = "SELECT *
//     FROM users          
//     JOIN users IN users.id = curriculumsubject.SubjectID 
//     JOIN curriculum IN curriculum.CurriculumID IN curriculumsubject.CurriculumID 
//     JOIN students IN student.track_id = curriculum.descriptive 
//     WHERE students.track_id == $track";  

//     return $qry;
// }

// function get_history(){
//     $sql = "SELECT * FROM audit";
//     $result = $conn->query($sql);
    
//     // Display data in a table
//     if ($result->num_rows > 0) {
//         echo "<table><tr><th>Audit ID</th><th>Table ID</th><th>Content ID</th><th>Action</th><th>Old Data</th><th>New Data</th><th>Modified By</th><th>Modified Date</th></tr>";
//         while($row = $result->fetch_assoc()) {
//             echo "<tr><td>".$row["AuditID"]."</td><td>".$row["TableID"]."</td><td>".$row["ContentID"]."</td><td>".$row["Action"]."</td><td>".$row["OldData"]."</td><td>".$row["NewData"]."</td><td>".$row["ModifiedBy"]."</td><td>".$row["ModifiedDate"]."</td></tr>";
//         }
//         echo "</table>";
//     } else {
//         echo "0 results";
//     }
// }
?>
