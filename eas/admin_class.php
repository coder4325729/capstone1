<?php
session_start();
ini_set('display_errors', 1);
Class Action {
	private $db;

	public function __construct() {
		ob_start();
   	include 'db_connect.php';
	include 'history.php';
	include 'query.php';
    $this->db = $conn;
	}
	function __destruct() {
	    $this->db->close();
	    ob_end_flush();
	}

	function login(){
		extract($_POST);
		$qry = $this->db->query("SELECT *, id, concat(firstname,' ',lastname) as name FROM users where username = '".$username."' and password = '".md5($password)."' and type= 1 ");
		if($qry->num_rows > 0){
			foreach ($qry->fetch_array() as $key => $value) {
				if($key != 'password' && !is_numeric($key))
					$_SESSION['login_'.$key] = $value;
			}
			// Get user ID and call get_history
			$user_id = $_SESSION['login_id']; // Assuming 'id' is the user ID field
			get_history($this->db, $user_id);
	
			return 1;
		}else{
			return 2;
		}
	}
	function logout(){
		session_destroy();
		foreach ($_SESSION as $key => $value) {
			unset($_SESSION[$key]);
		}
		header("location:login.php");
	}
	function login2(){
		extract($_POST);
			$qry = $this->db->query("SELECT *,concat(lastname,', ',firstname,' ',middlename) as name FROM students where student_code = '".$student_code."' ");
		if($qry->num_rows > 0){
			foreach ($qry->fetch_array() as $key => $value) {
				if($key != 'password' && !is_numeric($key))
					$_SESSION['rs_'.$key] = $value;
			}
				return 1;
		}else{
			return 3;
		}
	}
	function save_user(){
		extract($_POST);
		$data = "";
		foreach($_POST as $k => $v){
			if(!in_array($k, array('id','cpass','password')) && !is_numeric($k)){
				if(empty($data)){
					$data .= " $k='$v' ";
				}else{
					$data .= ", $k='$v' ";
				}
			}
		}
		if(!empty($cpass) && !empty($password)){
					$data .= ", password=md5('$password') ";

		}
		$check = $this->db->query("SELECT * FROM users where email ='$email' ".(!empty($id) ? " and id != {$id} " : ''))->num_rows;
		if($check > 0){
			return 2;
			exit;
		}
		if(isset($_FILES['img']) && $_FILES['img']['tmp_name'] != ''){
			$fname = strtotime(date('y-m-d H:i')).'_'.$_FILES['img']['name'];
			$move = move_uploaded_file($_FILES['img']['tmp_name'],'../assets/uploads/'. $fname);
			$data .= ", avatar = '$fname' ";

		}
		if(empty($id)){
			$save = $this->db->query("INSERT INTO users set $data");
		}else{
			$save = $this->db->query("UPDATE users set $data where id = $id");
		}

		if($save){
			return 1;
		}
	}
	function signup(){
		extract($_POST);
		$data = "";
		foreach($_POST as $k => $v){
			if(!in_array($k, array('id','cpass')) && !is_numeric($k)){
				if($k =='password'){
					if(empty($v))
						continue;
					$v = md5($v);

				}
				if(empty($data)){
					$data .= " $k='$v' ";
				}else{
					$data .= ", $k='$v' ";
				}
			}
		}

		$check = $this->db->query("SELECT * FROM users where email ='$email' ".(!empty($id) ? " and id != {$id} " : ''))->num_rows;
		if($check > 0){
			return 2;
			exit;
		}
		if(isset($_FILES['img']) && $_FILES['img']['tmp_name'] != ''){
			$fname = strtotime(date('y-m-d H:i')).'_'.$_FILES['img']['name'];
			$move = move_uploaded_file($_FILES['img']['tmp_name'],'../assets/uploads/'. $fname);
			$data .= ", avatar = '$fname' ";

		}
		if(empty($id)){
			$save = $this->db->query("INSERT INTO users set $data");

		}else{
			$save = $this->db->query("UPDATE users set $data where id = $id");
		}

		if($save){
			if(empty($id))
				$id = $this->db->insert_id;
			foreach ($_POST as $key => $value) {
				if(!in_array($key, array('id','cpass','password')) && !is_numeric($key))
					$_SESSION['login_'.$key] = $value;
			}
					$_SESSION['login_id'] = $id;
			return 1;
		}
	}

	function update_user(){
		extract($_POST);
		$data = "";
		foreach($_POST as $k => $v){
			if(!in_array($k, array('id','cpass','table')) && !is_numeric($k)){
				if($k =='password')
					$v = md5($v);
				if(empty($data)){
					$data .= " $k='$v' ";
				}else{
					$data .= ", $k='$v' ";
				}
			}
		}
		if($_FILES['img']['tmp_name'] != ''){
			$fname = strtotime(date('y-m-d H:i')).'_'.$_FILES['img']['name'];
			$move = move_uploaded_file($_FILES['img']['tmp_name'],'assets/uploads/'. $fname);
			$data .= ", avatar = '$fname' ";

		}
		$check = $this->db->query("SELECT * FROM users where email ='$email' ".(!empty($id) ? " and id != {$id} " : ''))->num_rows;
		if($check > 0){
			return 2;
			exit;
		}
		if(empty($id)){
			$save = $this->db->query("INSERT INTO users set $data");
		}else{
			$save = $this->db->query("UPDATE users set $data where id = $id");
		}

		if($save){
			foreach ($_POST as $key => $value) {
				if($key != 'password' && !is_numeric($key))
					$_SESSION['login_'.$key] = $value;
			}
			if($_FILES['img']['tmp_name'] != '')
			$_SESSION['login_avatar'] = $fname;
			return 1;
		}
	}
	function delete_user(){
		extract($_POST);
		$delete = $this->db->query("DELETE FROM users where id = ".$id);
		if($delete)
			return 1;
	}
	function save_system_settings(){
		extract($_POST);
		$data = '';
		foreach($_POST as $k => $v){
			if(!is_numeric($k)){
				if(empty($data)){
					$data .= " $k='$v' ";
				}else{
					$data .= ", $k='$v' ";
				}
			}
		}
		if($_FILES['cover']['tmp_name'] != ''){
			$fname = strtotime(date('y-m-d H:i')).'_'.$_FILES['cover']['name'];
			$move = move_uploaded_file($_FILES['cover']['tmp_name'],'../assets/uploads/'. $fname);
			$data .= ", cover_img = '$fname' ";

		}
		$chk = $this->db->query("SELECT * FROM system_settings");
		if($chk->num_rows > 0){
			$save = $this->db->query("UPDATE system_settings set $data where id =".$chk->fetch_array()['id']);
		}else{
			$save = $this->db->query("INSERT INTO system_settings set $data");
		}
		if($save){
			foreach($_POST as $k => $v){
				if(!is_numeric($k)){
					$_SESSION['system'][$k] = $v;
				}
			}
			if($_FILES['cover']['tmp_name'] != ''){
				$_SESSION['system']['cover_img'] = $fname;
			}
			return 1;
		}
	}
	function save_image(){
		extract($_FILES['file']);
		if(!empty($tmp_name)){
			$fname = strtotime(date("Y-m-d H:i"))."_".(str_replace(" ","-",$name));
			$move = move_uploaded_file($tmp_name,'../assets/uploads/'. $fname);
			$protocol = strtolower(substr($_SERVER["SERVER_PROTOCOL"],0,5))=='https'?'https':'http';
			$hostName = $_SERVER['HTTP_HOST'];
			$path =explode('/',$_SERVER['PHP_SELF']);
			$currentPath = '/'.$path[1]; 
			if($move){
				return $protocol.'://'.$hostName.$currentPath.'/assets/uploads/'.$fname;
			}
		}
	}
	function save_class(){
		extract($_POST);
		$data = "";
		foreach($_POST as $k => $v){
			if(!in_array($k, array('id')) && !is_numeric($k)){
				if(empty($data)){
					$data .= " $k='$v' ";
				}else{
					$data .= ", $k='$v' ";
				}
			}
		}
		$chk = $this->db->query("SELECT * FROM classes where level ='$level' and section = '$section' and id != '$id' ");
		if($chk->num_rows > 0){
			return 2;
			exit;
		}
		if(empty($id)){
			$save = $this->db->query("INSERT INTO classes set $data");
		}else{
			$save = $this->db->query("UPDATE classes set $data where id = $id");
		}
		if($save){
			return 1;
		}
	}
	function delete_class(){
		extract($_POST);
		$delete = $this->db->query("DELETE FROM classes where id = $id");
		if($delete){
			return 1;
		}
	}
	
	function save_curriculum(){
		extract($_POST);
		$data = "";
		foreach($_POST as $k => $v){
			if($k !== 'CurriculumID' && !is_numeric($k)){
				if(empty($data)){
					$data .= "$k='$v'";
				}else{
					$data .= ", $k='$v'";
				}
			}
		}
		if(isset($CurriculumID)){
			$chk = $this->db->query("SELECT * FROM curriculum WHERE CurriculumID = '$CurriculumID' ");
		}else{
			$chk = $this->db->query("SELECT * FROM curriculum");
		}
		if($chk->num_rows > 0){
			return 2;
		}
	
		if(isset($CurriculumID)){
			$save = $this->db->query("UPDATE curriculum SET $data WHERE CurriculumID = $CurriculumID");
		}else{
			$save = $this->db->query("INSERT INTO curriculum SET $data");
		}
		if($save){
			return 1;
		}
	}
	function delete_curriculum(){
		extract($_POST);
		$delete = $this->db->query("DELETE FROM curriculum where CurriculumID = $CurriculumID");
		if($delete){
			return 1;
		}
	}
	function save_subject(){
		extract($_POST);
		$data = "";
		foreach($_POST as $k => $v){
			if(!in_array($k, array('SubjectID')) && !is_numeric($k)){
				if(empty($data)){
					$data .= " $k='$v' ";
				}else{
					$data .= ", $k='$v' ";
				}
			}
		}
		$chk = $this->db->query("SELECT * FROM subjects where Pencode ='$Pencode' and SubjectID != '$SubjectID' ");
		if($chk->num_rows > 0){
			return 2;
			exit;
		}
		if(empty($SubjectID)){
			$save = $this->db->query("INSERT INTO subjects set $data");
		}else{
			$save = $this->db->query("UPDATE subjects set $data where SubjectID = $SubjectID");
		}
		if($save){
			return 1;
		}
	}
	function delete_subject(){
		extract($_POST);
		$delete = $this->db->query("DELETE FROM subjects where SubjectID = $SubjectID");
		if($delete){
			return 1;
		}
	}
	function get_curriculumsubject($syear, $StudentID) {
		$qry = "SELECT "
			. "subjects.SubjectID AS SubjectID, "
			. "subjects.Description AS Description, "
			. "subjects.Prerequisite AS Prerequisite, "
			. "curriculum.syear AS syear, "
			. "curriculum.semester AS Semester "
			. "FROM subjects "
			. "JOIN curriculumsubject ON subjects.SubjectID = curriculumsubject.SubjectID "
			. "JOIN curriculum ON curriculumsubject.CurriculumID = curriculum.CurriculumID "
			. "WHERE curriculum.syear = '$syear' "
			. "ORDER BY curriculumsubject.ordinality";
		// echo $qry;
		$rqry = "";
		$result = $this->db->query($qry);
	
		$electives = $this->get_specialization($StudentID, $syear);
	
		$i = 0;
		$j = 0;
		if ($result->num_rows > 0) {
			while ($row = $result->fetch_assoc()) {
				$rqry .= ($i == 0 ? "" : ",");
				$rqry .= "(" . $StudentID . ",";
				if (str_contains(strtolower($row['Description']), "elective")) {
					// Check if there are electives available
					if (!empty($electives) && isset($electives[$j])) {
						// Use the subject from the database
						$rqry .= $electives[$j]['SubjectID'] . ",";
						$rqry .= $row['syear'] . ",";
						$rqry .= $row['Semester'] . ",";
						// Determine Status based on Prerequisite of the elective
						$rqry .= (strtolower($electives[$j]['Prerequisite']) == "none" ? 0 : 1) . ")";
						$j++;
					} else {
						// Use the subject from the database
						$rqry .= $row['SubjectID'] . ",";
						$rqry .= $row['syear'] . ",";
						$rqry .= $row['Semester'] . ",";
						// Determine Status based on Prerequisite
						$rqry .= (strtolower($row['Prerequisite']) == "none" ? 0 : 1) . ")";
					}
				} else {
					// Use the subject from the database
					$rqry .= $row['SubjectID'] . ",";
					$rqry .= $row['syear'] . ",";
					$rqry .= $row['Semester'] . ",";
					// Determine Status based on Prerequisite
					$rqry .= (strtolower($row['Prerequisite']) == "none" ? 0 : 1) . ")";
				}
				$i++;
			}
		}
		return $rqry;
	}
	
	function get_specialization($StudentID, $syear) {
		$track = [];
		$qry = "SELECT * "
			. "FROM subjects "
			. "JOIN curriculumsubject ON curriculumsubject.SubjectID = subjects.SubjectID "
			. "JOIN curriculum ON curriculum.CurriculumID = curriculumsubject.CurriculumID "
			. "JOIN students ON students.track_id = curriculum.descriptive "
			. "WHERE students.id = $StudentID AND curriculum.syear = $syear "
			. "ORDER BY curriculumsubject.ordinality";
			
	
		$result = $this->db->query($qry);
	
		while ($row = $result->fetch_assoc()) {
			$track[] = $row;
		}
	
		return $track;
	}
	
	
	function insert_subjecttostudent($syear, $StudentID){
		// First, remove old data for the given StudentID
		$this->remove_old_syear_data($StudentID);
	
		// Insert new data for the given StudentID and syear
		$qry = "INSERT INTO studentcurriculumsubject (StudentID, SubjectID, syear, Semester, Status) VALUES "
			   .$this->get_curriculumsubject($syear, $StudentID);
		echo $qry; // For testing purposes
	
		$result = $this->db->query($qry);
	}
	
	function remove_old_syear_data($StudentID) {
		// Delete old data for the given StudentID
		$qry = "DELETE FROM studentcurriculumsubject WHERE StudentID = '$StudentID'";
		$this->db->query($qry);
	}
	
	


	function save_student(){
		extract($_POST);
		$data = "";
		$syear = 2024;
		foreach($_POST as $k => $v){
			if(!in_array($k, array('id','areas_id')) && !is_numeric($k)){
				
				if($k == 'description')
					$v = htmlentities(str_replace("'","&#x2019;",$v));
				if($k  == 'curriculum_id'){
					$syear = $v;
					echo $v;
				}
				if(empty($data)){
					$data .= " $k='$v' ";
				}else{
					$data .= ", $k='$v' ";
				}
			}
		}
		$chk = $this->db->query("SELECT * FROM students where student_code ='$student_code' and id != '$id' ")->num_rows;
		if($chk > 0){
			return $id."x";
		}
		if(empty($id)){
			$save = $this->db->query("INSERT INTO students set $data");
			$id = $this->db->insert_id; // Get the inserted id
			log_history($this->db, 4, $id, "Add Student", "", "$student_code", 1, date('Y-m-d H:i:s'));
		}else{
			$query = "SELECT * FROM students WHERE id = $id";
    $result = mysqli_query($this->db, $query);
    $row = mysqli_fetch_array($result);
    $_firstname = $row['firstname'];
    $_middlename = $row['middlename'];
    $_lastname = $row['lastname'];
    $_studentcode = $row['student_code'];
	$_curriculumid = $row['curriculum_id'];
	$_trackid= $row['track_id'];
     
			$save = $this->db->query("UPDATE students set $data where id = $id");
			$query = "SELECT * FROM students WHERE id = $id";
    $result = mysqli_query($this->db, $query);
    $row = mysqli_fetch_array($result);
    $firstname = $row['firstname'];
    $middlename = $row['middlename'];
    $lastname = $row['lastname'];
    $studentcode = $row['student_code'];
	$curriculumid = $row['curriculum_id'];
	$trackid= $row['track_id'];
			log_history($this->db, 4, $id, "Edit Student", "$_firstname, $_middlename, $_lastname, $_studentcode, $_curriculumid, $_trackid", "$firstname, $middlename, $lastname, $studentcode, $curriculumid, $trackid", 1, date('Y-m-d H:i:s'));
		
		}
		if($save){
			$this->insert_subjecttostudent($syear, $id);
			return $id."y";
		}
			}
	
			
				
	function delete_student(){
		extract($_POST);
		$delete = $this->db->query("DELETE FROM students where id = $id");
		if($delete){
			return 1;
		}
	}
	function insert_subject(){
		extract($_POST);
		$data = "";
		foreach($_POST as $k => $v){
			if(!in_array($k, array('CurriculumSubjectID')) && !is_numeric($k)){
				if(empty($data)){
					$data .= " $k='$v' ";
				}else{
					$data .= ", $k='$v' ";
				}
			}
		}
	
		// Check if the combination of CurriculumID and SubjectID already exists in the curriculumsubject table
		$chk = $this->db->query("SELECT * FROM curriculumsubject where CurriculumID = '$CurriculumID' AND SubjectID = '$SubjectID'");
		if($chk->num_rows > 0){
			return 2; // Combination already exists
		}
	
		// Insert the CurriculumID and SubjectID into the curriculumsubject table
		$save = $this->db->query("INSERT INTO curriculumsubject (CurriculumID, SubjectID) VALUES ('$CurriculumID', '$SubjectID')");
		if($save){
			return 1; // Success
		} else {
			return 0; // Error
		}
	}
	function delete_CurriculumSubject(){
		extract($_POST);
		$delete = $this->db->query("DELETE FROM curriculumsubject where CurriculumSubjectID = $CurriculumSubjectID");
		if($delete){
			return 1;
		}
	}
	
	function save_StudentCurriculumSubject(){
		extract($_POST);
		$data = "";
		foreach($_POST as $k => $v){
			if(!in_array($k, array('StudentCurriculumSubjectID')) && !is_numeric($k)){
				if(empty($data)){
					$data .= " $k='$v' ";
				}else{
					$data .= ", $k='$v' ";
				}
			}
		}

		
		if($Prerequisite = "None" ){
			$data .= ", Status= 1 ";
		}else{
			$data .= ", Status= 0 ";

		}
		$chk = $this->db->query("SELECT * FROM studentcurriculumsubject where StudentCurriculumSubjectID ='$StudentCurriculumSubjectID'");
		if($chk->num_rows > 0){
			return 2;
			exit;
		}
		if(empty($StudentCurriculumSubjectID)){
			$save = $this->db->query("INSERT INTO studentcurriculumsubject set $data");
		}else{
			$save = $this->db->query("UPDATE studentcurriculumsubject set $data where StudentCurriculumSubjectID = $StudentCurriculumSubjectID");
		}
		if($save){
			return 1;
		}
	}
	function delete_StudentCurriculumSubject(){
		extract($_POST);
		$delete = $this->db->query("DELETE FROM studentcurriculumsubject where StudentCurriculumSubjectID = $StudentCurriculumSubjectID");
		if($delete){
			return 1;
		}
	}
	
			
	

	function save_result(){
		extract($_POST);
		$data = "";
		foreach($_POST as $k => $v){
			if(!in_array($k, array('id','mark','subject_id')) && !is_numeric($k)){
				if(empty($data)){
					$data .= " $k='$v' ";
				}else{
					$data .= ", $k='$v' ";
				}
			}
		}
		$chk = $this->db->query("SELECT * FROM results where student_id ='$student_id' and class_id='$class_id' and id != '$id' ");
		if($chk->num_rows > 0){
			return 2;
			exit;
		}
		if(empty($id)){
			$save = $this->db->query("INSERT INTO results set $data");
		}else{
			$save = $this->db->query("UPDATE results set $data where id = $id");
		}
		if($save){
				$id = empty($id) ? $this->db->insert_id : $id;
				$this->db->query("DELETE FROM result_items where result_id = $id");
				foreach($subject_id as $k => $v){
					$data= " result_id = $id ";
					$data.= ", subject_id = $v ";
					$data.= ", mark = '{$mark[$k]}' ";
					$this->db->query("INSERT INTO result_items set $data");
				}
				return 1;
		}
	}
	function delete_result(){
		extract($_POST);
		$delete = $this->db->query("DELETE FROM results where id = $id");
		if($delete){
			return 1;
		}
	}
	
}



