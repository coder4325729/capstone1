<?php
function get_subjectsbytrack($track) {
    $qry = "SELECT *"
         . "FROM subjects "         
         . "JOIN subjects IN subjects.SubjectID = curriculumsubject.SubjectID "
         . "JOIN curriculum IN curriculum.CurriculumID IN curriculumsubject.CurriculumID "
         . "JOIN students IN student.track_id = curriculum.descriptive "
         . "WHERE students.track_id == $track";  
    
         return $qry;
}

function console_log($output, $with_script_tags = true) {
    $js_code = 'console.log(' . json_encode($output, JSON_HEX_TAG) . ');';
    if ($with_script_tags) {
        $js_code = '<script>' . $js_code . '</script>';
    }
    echo $js_code;
}
?>
