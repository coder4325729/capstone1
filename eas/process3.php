<?php
// Include your database connection file
include 'db_connect.php';
include 'history.php';
// Check if the form is submitted
if ($_SERVER["REQUEST_METHOD"] == "POST") {
    // Retrieve form data
    $syear = $_POST['syear'];
    $descriptive = $_POST['descriptive'];
    $semester = $_POST['semester'];
    $track = $_POST['track'];

    // Perform SQL query to insert data into the curriculum table
    $query = "INSERT INTO curriculum (syear, descriptive, semester, track) VALUES ('$syear', '$descriptive', '$semester', '$track')";
    $result = $conn->query($query);

    // Check if the query was successful
    if ($result) {
        echo "Curriculum added successfully";
    } else {
        echo "Error inserting into curriculum: " . $conn->error;
    }
}

// Close database connection
$conn->close();

?>
