<?php
// Connect to the database
$conn = mysqli_connect("localhost", "root", "", "eas_db");

// Get the ID of the content to be deleted
$id = $_GET['id'];

// Prepare and execute the deletion query
$sql = "DELETE FROM table_data WHERE id = ?";
$stmt = mysqli_prepare($conn, $sql);
mysqli_stmt_bind_param($stmt, "i", $id);
mysqli_stmt_execute($stmt);

// Check if deletion was successful
if (mysqli_affected_rows($conn) > 0) {
    header("Location: index.php?deleted=true"); // Redirect to the main page with a success message
} else {
    header("Location: index.php?error=true"); // Redirect with an error message
}

// Close the connection
mysqli_close($conn);
?>