<?php
function select_CurriculumSubject($schoolyear, $semester){
    $qry = "SELECT "
         . "subjects.Pencode, "
         . "subjects.Description, "
         . "subjects.Lab, "
         . "subjects.Lec, "
         . "subjects.SubjectID, "
         . "subjects.Prerequisite, "
         . "curriculum.descriptive, "
         . "curriculum.CurriculumID, "
         . "curriculumsubject.CurriculumSubjectID "
         . "FROM subjects "
         . "JOIN curriculumsubject ON subjects.SubjectID = curriculumsubject.SubjectID "
         . "JOIN curriculum ON curriculumsubject.CurriculumID = curriculum.CurriculumID "
         . "WHERE curriculum.syear = $schoolyear AND curriculum.semester = $semester";
    return $qry;
}

function console_log($output, $with_script_tags = true) {
    $js_code = 'console.log(' . json_encode($output, JSON_HEX_TAG) . ');';
    if ($with_script_tags) {
        $js_code = '<script>' . $js_code . '</script>';
    }
    echo $js_code;
}
?>
