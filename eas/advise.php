<?php include 'db_connect.php';
include 'query2.php'; 
include 'gethighlight.php';

// Check if the student ID is provided in the URL
if (isset($_GET['id'])) {
    // Retrieve the student ID from the URL
    $student_id = $_GET['id'];

} else {
    // Handle the case when the student ID is not provided in the URL
    echo "Student ID not provided.";
    exit; // Exit PHP execution
}

$student_curriculum_query = $conn->query("SELECT curriculum_id FROM students WHERE id = $student_id");
$row = $student_curriculum_query->fetch_assoc();
$curriculum_id = $row['curriculum_id'];

// Query to retrieve the school year based on the curriculum ID
$schoolyear_query = $conn->query("SELECT syear FROM curriculum WHERE syear = $curriculum_id");
$row = $schoolyear_query->fetch_assoc();
$schoolyear = $row['syear'];

// Check if the form is submitted
$status_name = array("None", "None", "NC", "Passed", "Failed", "FA", "INC");
$status_color = array("pencodegreen", "pencodered", "pencodered", "pencodeblack", "pencodegreen", "pencodered", "pencodeyellow");
?>

<style>
 .btn-square {
    width: 50px; /* Adjust the width as needed */
    height: 50px; /* Same as the width to make it square */
    border-radius: 0;
    background-color: green; /* Removes rounded corners */
}

/* Style for the fixed button */
#saveGradesBtn {
    position: fixed;
    bottom: 20px; /* Adjust the distance from the bottom */
    right: 20px; /* Adjust the distance from the right */
    z-index: 9999; /* Ensure the button stays above other content */
}
.pencodegreen{
 color: green;
}
.pencodeblack{
 color: black;
}
.pencodered{
 color: red;
}
.pencodeyellow{
 color: darkgoldenrod;
}
@media print {
    th.action,
    td.action {
        display: none;
    }
    .no-print {
        display: none !important;
    }
}
</style>

<div>
<div class="no-print">
    
<button id="printButton" class="btn" style="background-color: #1F3761; color: white;" id="printButton" data-id="<?php echo $student_id; ?>">Print</button>

</div>
    <div class="card card-outline card-secondary">
        <div class="card-header">
            <div class="card-tools">
            </div>
        </div>
       
            <div class="card-body row">
            <div class="col-12">
            </div>
            <?php
        // Loop through each curriculum
        for ($i = 1; $i <= 8; $i++) {
            if ($i > 8) {
                // Fetch the curriculum data from the database for the descriptive column with a specific condition
                $curriculumTitle_query = $conn->query("SELECT descriptive FROM curriculum WHERE semester = $i AND syear = $schoolyear");
                $curriculumTitle_row = $curriculumTitle_query->fetch_assoc();
                $curriculumTitle = isset($curriculumTitle_row['descriptive']) ? $curriculumTitle_row['descriptive'] : 'No data available';
            } else {
                $curriculumTitle = "Year " . ceil($i / 2) . " - " . ($i % 2 == 1 ? "First" : "Second") . " Semester";
            }
            
            // Fetch data from the database
            $qry = $conn->query(select_StudentCurriculumSubject(2024, $i, $student_id));
            $r = array();
            while ($row = $qry->fetch_assoc()) {
                if (empty($r)) {
                    $r = [$row['Pencode'] => $row];
                } else {
                    if  (array_key_exists($row['Pencode'], $r)) {
                        $postreq = "," . $row['Postrequisite'];
                        $r[$row['Pencode']]['Postrequisite'] .= $postreq;
                    } else {
                        $r[$row['Pencode']] = $row;
                    }
                }
            }
            
            // Check if there are any rows fetched for this curriculum
            if (!empty($r)) {
        ?>
                <div class="col-6">
                    <h5><?php echo $curriculumTitle; ?></h5>
                    <div class="table-responsive">
                        <table class="table tabe-hover table-bordered" id="list_<?php echo $i; ?>">
                            <thead>
                                <tr>
                                    <th>Pen Code</th>
                                    <th>Descriptive Title</th>
                                    <th>Lec</th>
                                    <th>Lab</th>
                                    <th>Total</th>
                                    <th>Pre-requisite</th>
                                    <th>Grade</th>
                                    <th>Status</th>
                                    <th class= "action">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                            <?php
                            foreach ($r as $key => $value) {
                                $row = $value;


                                
                            ?>
                                <tr>
                                    <td class="col-2" id="penCodeColumn_<?php echo formatpencode($row['Pencode']) ?>">
                                        <b id="pencode_<?php echo formatpencode($row['Pencode']) ?>" class="pencode <?php echo $status_color[$row['Status']] ?>" data-pencode-id ="<?php echo $row['Pencode'] ?>"><?php echo ucwords($row['Pencode']) ?></b>
                                    </td>
                                    <td class="col-4"><?php echo ucwords($row['Description']) ?></td>
                                    <td class="col-1"><b><?php echo ucwords($row['Lec']) ?></b></td>
                                    <td class="col-1"><?php echo ucwords($row['Lab']) ?></td>
                                    <td class="col-1"><?php echo ucwords($row['Lab'] + $row['Lec']) ?></td>
                                    <td class="col-1" data-postrequisite-id="<?php echo formatpencode($row['Postrequisite'])?>" id="prerequisite_<?php echo formatpencode($row['Pencode'])?>" data-prerequisite-id="<?php echo formatpencode($row['Prerequisite'])?>"><?php echo ucwords($row['Prerequisite']) ?></td>
                                    <td class="col-1 grade_column" contenteditable="false" id="grade_<?php echo formatpencode($row['Pencode']) ?>" data-grade-id="<?php echo $row['StudentCurriculumSubjectID'] ?>"><?php echo $row['Grade']?></td>
                                    <td class="col-1 status_column"><b><?php echo ucwords($status_name[$row['Status']]) ?></b></td>
                                    <td class="text-center col-1 action">
                                        <div class="btn-group">
                                            <!-- Edit button -->
                                            <button type="button" class="btn btn-info btn-flat edit_grade" data-id="<?php echo $row['StudentCurriculumSubjectID'] ?>">
                                                <i class="fas fa-edit"></i>
                                            </button>
                                            <!-- Save button (hidden by default) -->
                                            <button type="button" class="btn btn-success btn-flat save_grade" pencode="<?php echo $row['Pencode'] ?>" postrequisite="<?php echo formatpencode($row['Postrequisite'])?>" prerequisite= "<?php echo formatpencode($row['Prerequisite'])?>"  data-id="<?php echo $row['StudentCurriculumSubjectID'] ?>" style="display: none;">
                                                <i class="fas fa-save"></i>
                                            </button>
                                            <!-- Delete button -->
                                            <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
               
            </button>
            <div class="dropdown-menu">
    <?php
    // Fetch audit records from the database for the specific StudentCurriculumSubjectID
    $audit_query = $conn->query("SELECT audit.AuditID, audit.Action, audit.OldData, audit.NewData, audit.ModifiedDate, users.firstname, users.username FROM audit JOIN users ON users.id = audit.ModifiedBy WHERE audit.ContentID = '".$row['StudentCurriculumSubjectID']."'");
    // Loop through each audit record
    while ($audit_row = $audit_query->fetch_assoc()):
    ?>
        <a class="dropdown-item" href="#">Last Modified By - <?php echo $audit_row['username']; ?></a>
    <?php endwhile; ?>
</div>
                                      </div>
                                      
                                    </td>
                                </tr>
                            <?php } ?>
                            </tbody>
                        </table>
                    </div>
                </div>
        <?php
            }
        }
        ?>
    </div>
</div>

<script>
$(document).ready(function(){
    // Event handler for edit button click
    $(document).on('click', '.edit_grade', function() {
        var row = $(this).closest('tr'); // Get the closest row to the clicked button
        var gradeCell = row.find('.grade_column'); // Find the grade cell within that row
        gradeCell.prop('contenteditable', true); // Enable editing for the grade cell
        gradeCell.focus(); // Focus on the grade cell
        $(this).hide(); // Hide the edit button
        $(this).siblings('.save_grade').show(); // Show the save button
    });

    // Event handler for save button click
    $(document).on('click', '.save_grade', function() {
        var gradeCell = $(this).closest('tr').find('[contenteditable="true"]');
        var statusCell = $(this).closest('tr').find('.status_column'); // Get the status cell
        var studentCurriculumSubjectID = $(this).data('id');
        var grade = gradeCell.text().trim();
        var pencode = $(this).attr('pencode');
        var prerequisite = $(this).attr('prerequisite');
        var postrequisite = $(this).attr('postrequisite').split(',');
       
        // Disable editing of the grade cell
        gradeCell.prop('contenteditable', false);

        // Hide the save button and show the edit button
        $(this).hide();
        $(this).siblings('.edit_grade').show();

        // Send the updated grade to the server for saving
        $.ajax({
            url: 'process2.php',
            method: 'POST',
            data: {
                StudentCurriculumSubjectID: studentCurriculumSubjectID,
                Grade: grade
                

            },
            success: function(resp) {
                $.ajax({
                url: 'get_status.php', // Change to the actual URL for fetching status
                method: 'POST',
                data: {
                    StudentCurriculumSubjectID: studentCurriculumSubjectID
                },
                success: function(status) {
                    // Update the status cell with the new status
                     statusCell.html(status); // Use html() instead of text() to interpret HTML tags
    },
                error: function(xhr, status, error) {
                    console.error(xhr.responseText);
                }
            }); 
            },
            error: function(xhr, status, error) {
                console.error(xhr.responseText);
            }
        });
    });


    $('#list').dataTable();

    $('.delete_StudentCurriculumSubject').click(function(){
        var studentCurriculumSubjectID = $(this).attr('data-id');
        if(confirm("Are you sure to delete this Subject?")) {
            $.ajax({
                url: 'delete_scs.php',
                method: 'POST',
                data: { StudentCurriculumSubjectID: studentCurriculumSubjectID },
                success: function(resp){
                    alert(resp);
                    location.reload();
                },
                error: function(xhr, status, error) {
                    console.error(xhr.responseText);
                }
            });
        }
    });

    $(document).on('click', '.manage_grade', function(){
        uni_modal("Manage Grade","manage_grade.php?StudentCurriculumSubjectID="+$(this).attr('data-id'))
    });

    $('.insert_status').click(function(){
        uni_modal("Set Status","insert_grade.php?StudentCurriculumSubjectID="+$(this).attr('data-id'));
    });
    function updatePencodeColor(pencode, grade, is_postreq = false) {
  // Clear previous highlight explicitly
  $("#pencode_" + pencode).removeClass("pencodeyellow");
  $("#pencode_" + pencode).removeClass("pencodered");
  $("#pencode_" + pencode).removeClass("pencodegreen");
  $("#pencode_" + pencode).removeClass("pencodeblack");

  var color = "";

  // Check for numeric grade first
  if (!isNaN(parseFloat(grade))) {
    var numericGrade = parseFloat(grade);
    if (numericGrade > 0 && numericGrade <= 3) {
      color = "pencodeblack"; // Black for pencode (passing grade 0-3)
    } else if (numericGrade > 3) {
      color = "pencodegreen"; // Green for pencode (grades > 3)
    }
  } else {
    // Check for special grades (INC, NC, FA)
    if (grade.toLowerCase() === "inc") {
      color = "pencodeyellow";
    } else if (grade.toLowerCase() === "nc" || grade.toLowerCase() === "fa") {
      color = "pencodered";
    }
  }

  // Separate logic for postrequisite (green for <= 3, red for > 3)
  if (is_postreq) {
    if (numericGrade <= 3) {
      color = "pencodegreen"; // Green for postreq (passing grade)
    } else {
      color = "pencodered"; // Red for postreq (grades > 3)
    }
  }

  // Add the color class to the Pencode cell
  $("#pencode_" + pencode).addClass(color);
}



    function updatepostreq(pencode, grade){
        pencode
    }

    function clearpencodecolor(pencode){
        $("#pencode_"+ pencode).removeClass("pencodered pencodegreen pencodeblack")
    }

    function updatepostreq(pencode, grade){
        pencode
    }


    function pencodetopostrequisite(pencode, postrequisite) {
        var similarPostrequisites = {}; 

        if (!similarPostrequisites[pencode]) {
            similarPostrequisites[pencode] = postrequisite;
        } else {

            similarPostrequisites[pencode] += ', ' + postrequisite;
        }

        console.log('Pencode:', pencode, 'Postrequisites:', similarPostrequisites[pencode]);
    }


    $('[contenteditable="false"]').on('input', function() {
        var studentCurriculumSubjectID = $(this).data('grade-id');
        var gradeInput = this;
        var pencode = $(this).attr('id');
        pencode = pencode.replace("grade_", "");
        var grade = $(this).text();
        var prerequisite = $(this).siblings('td[data-prerequisite-id]').data('prerequisite-id');
        var postrequisites = $(this).siblings('td[data-postrequisite-id]').data('postrequisite-id')
        var postrequisite = postrequisites.split(',');
    
        updatePencodeColor(pencode, grade);
        for (p in postrequisite){
            updatePencodeColor(postrequisite[p], grade, true);
        }

        // Prepare the formData to be sent to the server
        var formData = { 
            StudentCurriculumSubjectID: studentCurriculumSubjectID, 
            Grade: grade 
        };
     
        // Send the formData to the server for processing
        $.ajax({
        url: 'process2.php',
        method: 'POST',
        data: formData,
        success: function(resp) {
            console.log(resp);
        },
        error: function(xhr, status, error) {
            console.error(xhr.responseText);
        }
    });
});

    function getPenCodeIdByPrerequisite(prerequisiteId) {
        var penCodeId = null; 

        $("#list_<?php echo $i; ?> tbody tr").each(function() {
            var prerequisiteTd = $(this).siblings('td[data-prerequisite-id="' + prerequisiteId + '"]').data('prerequisite-id');  

            if (prerequisiteTd.length) { 
                penCodeId = $(this).find('td:first-child').attr('id'); 
                return false; 
            }
        });

        return penCodeId; 
    }
});

document.getElementById('printButton').addEventListener('click', function() {
    var studentId = this.getAttribute('data-id');
    var printWindow = window.open('print.php?id=' + studentId, '_blank');
    printWindow.onload = function() {
// Hide date/time and text for printing
        printWindow.document.querySelectorAll('.no-print').forEach(function(element) {
            element.style.display = 'none';
        });
        printWindow.print();
    };
});

</script>
