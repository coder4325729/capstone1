<?php
// Include your database connection file
include 'db_connect.php';

// Check if the form is submitted
if ($_SERVER["REQUEST_METHOD"] == "POST") {
    // Retrieve form data
    $CurriculumID = $_POST['CurriculumID'];
    $SubjectIDs = $_POST['SubjectIDs']; // Notice the 'SubjectIDs' instead of 'SubjectID'

    // Set initial ordinality value
    $ordinality = 1;

    // Loop through each SubjectID and insert into curriculumsubject table
    foreach ($SubjectIDs as $SubjectID) {
        // Perform SQL query to insert data into the curriculumsubject table
        $query = "INSERT INTO curriculumsubject (CurriculumID, SubjectID, ordinality) VALUES ('$CurriculumID', '$SubjectID', '$ordinality')";
        $result = $conn->query($query);

        // Check if the query was successful
        if (!$result) {
            // Error in SQL query
            echo "Error: " . $conn->error;
            // Exit the loop if an error occurs
            break;
        }

        // Increment ordinality for the next insertion
        $ordinality++;
    }

    // Close database connection
    $conn->close();
}
?>
