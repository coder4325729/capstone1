<?php
include 'db_connect.php';
if(isset($_GET['CurriculumID'])){
	$qry = $conn->query("SELECT * FROM curriculum where CurriculumID ={$_GET['CurriculumID']}")->fetch_array();
	foreach($qry as $k => $v){
		$$k = $v;
	}
}
?>
<div class="container-fluid">
	<form action="" id="curriculum-form" method="post">
		<input type="hidden" name="CurriculumID" value="<?php echo isset($CurriculumID) ? $CurriculumID : '' ?>">
		<div id="msg" class="form-group"></div>
		
		<div class="form-group">
			<label for="syear" class="control-label">School Year</label>
			<input type="text" class="form-control form-control-sm" name="syear" id="syear" value="<?php echo isset($syear) ? $syear : '' ?>">
		</div>
		<div class="form-group">
			<label for="semester" class="control-label">Semester</label>
            <input type="int" class="form-control form-control-sm" name="semester" id="semester" value="<?php echo isset($semester) ? $semester : '' ?>">
		</div>
		
		<div class="form-group">
			<label for="descriptive" class="control-label">Description</label>
            <textarea name="descriptive" id="descriptive" cols="30" rows="4" class="form-control"><?php echo isset($descriptive) ? $descriptive : '' ?></textarea>
		</div>
		
	
	</form>
</div>

<!-- Success Modal -->
<div class="modal fade" id="successModal" tabindex="-1" aria-labelledby="successModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="successModalLabel">Success</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                Data successfully saved.
            </div>
        </div>
    </div>
</div>

<script>
    $(document).ready(function(){
        $('#curriculum-form').submit(function(e){
            e.preventDefault();
            start_load();
            var formData = $('#curriculum-form').serialize();
            $.ajax({
                url: 'process.php',
                method: 'POST',
                data: formData,
                success: function(response){
                    $('#successModal').modal('show');
                    end_load();
                }
            });
        });
    });
</script>