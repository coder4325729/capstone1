<?php
// Connect to the database
$conn = mysqli_connect("localhost", "root", "", "eas_db");

// Check the connection
if (!$conn) {
    die("Connection failed: " . mysqli_connect_error());
}

$htmlContent = $_POST['htmlContent'];
$cyear = $_POST['cyear'];

$cyear = mysqli_real_escape_string($conn, $cyear);
// Prepare SQL query
$stmt = $conn->prepare("INSERT INTO table_data (content, cyear) VALUES (?, ?)");
$stmt->bind_param("ss", $htmlContent, $cyear);

// Execute query
if ($stmt->execute()) {
    $response = array('status' => 'success', 'message' => 'HTML content saved successfully!');
} else {
    $response = array('status' => 'error', 'message' => 'Error saving HTML content: ' . $stmt->error);
}

// Close statement and connection
$stmt->close();
$conn->close();

// Send JSON response
header('Content-Type: application/json');
echo json_encode($response);
?>