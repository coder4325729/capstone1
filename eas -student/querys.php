<?php
function select_StudentCurriculumSubject($schoolyear, $semester, $StudentID) {
    $qry = "SELECT "
    . "subjects.PenCode AS Pencode, "
    . "subjects.Description AS Description, "
    . "subjects.Lab AS Lab, "
    . "subjects.Lec AS Lec, "
    . "subjects.Prerequisite AS Prerequisite, "
    . "studentcurriculumsubject.Grade AS Grade, "
    . "studentcurriculumsubject.Status AS Status, "
    . "studentcurriculumsubject.StudentCurriculumSubjectID AS StudentCurriculumSubjectID "
 . "FROM subjects "
 . "JOIN studentcurriculumsubject ON subjects.SubjectID = studentcurriculumsubject.SubjectID "
 . "WHERE studentcurriculumsubject.Semester = '$semester' "
 . "AND studentcurriculumsubject.StudentID = '$StudentID' ";

return $qry;
}

function console_log($output, $with_script_tags = true) {
    $js_code = 'console.log(' . json_encode($output, JSON_HEX_TAG) . ');';
    if ($with_script_tags) {
        $js_code = '<script>' . $js_code . '</script>';
    }
    echo $js_code;
}
?>
 